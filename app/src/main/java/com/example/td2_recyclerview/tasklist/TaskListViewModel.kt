package com.example.td2_recyclerview.tasklist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.td2_recyclerview.network.TasksRepository
import kotlinx.coroutines.launch

class TaskListViewModel : ViewModel() {
    private val repository = TasksRepository()

    private val _tasksList = MutableLiveData<List<Task>>()
    public val taskList: LiveData<List<Task>> = _tasksList

    fun loadTasks() {
        viewModelScope.launch {
            _tasksList.value = repository.loadTasks()!!
        }
    }

    fun deleteTask(task: Task) {
        viewModelScope.launch {
            repository.removeTask(task)
            val position = _tasksList.value.orEmpty().indexOfFirst { task.id == it.id }
            val editableList = _tasksList.value.orEmpty().toMutableList()
            editableList.removeAt(position)
            _tasksList.value = editableList
        }
    }

    fun addTask(task: Task) {
        viewModelScope.launch {
            val editableList = _tasksList.value.orEmpty().toMutableList()
            editableList.add(repository.createTask(task)!!)
            _tasksList.value = editableList
        }
    }

    fun editTask(task: Task) {
        viewModelScope.launch {
            val editedTask = repository.updateTask(task)!!
            val editableList = _tasksList.value.orEmpty().toMutableList()
            val position = editableList.indexOfFirst { task.id == it.id }
            editableList[position] = editedTask
            _tasksList.value = editableList
        }
    }
}