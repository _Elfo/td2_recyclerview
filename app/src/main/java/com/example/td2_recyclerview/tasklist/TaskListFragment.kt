package com.example.td2_recyclerview.tasklist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.td2_recyclerview.AuthenticationFragments.AuthenticationActivity
import com.example.td2_recyclerview.AuthenticationFragments.AuthenticationFragment
import com.example.td2_recyclerview.Login.Constants
import com.example.td2_recyclerview.R
import com.example.td2_recyclerview.network.Api
import com.example.td2_recyclerview.task.TaskActivity
import com.example.td2_recyclerview.userinfo.UserInfoActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.launch

class TaskListFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var infoTextView: TextView
    private lateinit var profilePicture: ImageView

    private val taskIntent by lazy {
        Intent(activity, TaskActivity::class.java)
    }
    private val userIntent by lazy {
        Intent(activity, UserInfoActivity::class.java)
    }

    private val authenticationIntent by lazy {
        Intent(activity, AuthenticationActivity::class.java)
    }

    private val adapter = TaskListAdapter()

    private val viewModel : TaskListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_task_list, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        infoTextView = view.findViewById(R.id.info_text_view)

        profilePicture = view.findViewById(R.id.profile_picture)
        profilePicture.setOnClickListener {
            startActivity(userIntent)
        }

        val logoutButton = view.findViewById<ImageButton>(R.id.log_out)
        logoutButton.setOnClickListener {
            PreferenceManager.getDefaultSharedPreferences(context).edit {
                putString(Constants.SHARED_PREF_TOKEN_KEY, "")
            }

            startActivity(authenticationIntent)
        }

        recyclerView = view.findViewById(R.id.recycler_view)

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter

        viewModel.taskList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        adapter.onDeleteClickListener = { task ->
            viewModel.deleteTask(task)
        }

        adapter.onEditClickListener = { task ->
            taskIntent.putExtra(Task.TASK_KEY, task)
            taskIntent.putExtra(Task.POSITION_KEY, adapter.currentList.indexOf(task))
            taskStartForResult.launch(taskIntent)
        }

        val addButton = view.findViewById<FloatingActionButton>(R.id.addButton)
        addButton.setOnClickListener {
            taskIntent.putExtra(Task.TASK_KEY, null as? Task?)
            taskIntent.putExtra(Task.POSITION_KEY, -1)
            taskStartForResult.launch(taskIntent)
        }
    }

    private val taskStartForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.data != null) {
                    val task = it.data!!.getSerializableExtra(Task.TASK_KEY) as Task
                    val position = it.data!!.getSerializableExtra(Task.POSITION_KEY) as Int

                    if (position == -1) {
                        viewModel.addTask(task)
                    }
                    else {
                        viewModel.editTask(task)
                    }
                }
            }

//    private val userInfoStartForResult =
//            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
//
//            }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            val userInfo = Api.INSTANCE.userWebService.getInfo().body()!!
            infoTextView.text = "${userInfo.firstName} ${userInfo.lastName}"
            profilePicture.load(userInfo.avatar) {
                transformations(CircleCropTransformation())
            }
        }
        viewModel.loadTasks()
    }
}
