package com.example.td2_recyclerview.tasklist

import kotlinx.serialization.SerialName
import java.io.Serializable

@kotlinx.serialization.Serializable
data class Task(
        @SerialName("id")
        val id: String,
        @SerialName("title")
        var title : String,
        @SerialName("description")
        var description : String = "Nouvelle tache"
) : Serializable {
    companion object {
        const val ADD_TASK_REQUEST_CODE = 666
        const val TASK_KEY = "task_key"
        const val POSITION_KEY = "position_key"
    }
}