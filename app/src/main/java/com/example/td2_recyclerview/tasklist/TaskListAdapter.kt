package com.example.td2_recyclerview.tasklist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.td2_recyclerview.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

//RecyclerView.Adapter<TaskListAdapter.TaskViewHolder>()

class TaskListAdapter : ListAdapter<Task, TaskListAdapter.TaskViewHolder>(diffCallback) {
    var onDeleteClickListener : ((Task) -> Unit)? = null

    var onEditClickListener : ((Task) -> Unit)? = null

    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(task: Task) {
            itemView.apply { // `apply {}` permet d'éviter de répéter `itemView.*`
                val textView = findViewById<TextView>(R.id.task_title)
                textView.text = task.title

                val supButton = findViewById<ImageButton>(R.id.supButton)
                supButton.setOnClickListener {
                    onDeleteClickListener?.invoke(task)
                }

                val editButton = findViewById<ImageButton>(R.id.editButton)
                editButton.setOnClickListener {
                    onEditClickListener?.invoke(task)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return TaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}

object diffCallback : DiffUtil.ItemCallback<Task>() {
    override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
        return oldItem == newItem
    }

}
