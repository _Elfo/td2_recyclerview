package com.example.td2_recyclerview.userinfo

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import coil.load
import coil.transform.CircleCropTransformation
import com.example.td2_recyclerview.BuildConfig
import com.example.td2_recyclerview.R
import com.example.td2_recyclerview.network.Api
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class UserInfoActivity : AppCompatActivity() {

    private lateinit var uploadImageButton: Button
    private lateinit var takePictureButton: Button

    private lateinit var urlPicture: String

    private val profilePicture: ImageView by lazy {
        findViewById(R.id.profile_picture)
    }

    private val photoUri by lazy {
        FileProvider.getUriForFile(
                this,
                BuildConfig.APPLICATION_ID + ".fileprovider",
                File.createTempFile("avatar", ".jpeg", externalCacheDir)
        )
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) openCamera()
            else showExplanationDialog()
        }

//    private val takePicture =
//            registerForActivityResult(ActivityResultContracts.TakePicturePreview()) { picture ->
//                if (picture != null) {
//                    val tmpFile = File.createTempFile("avatar", "jpeg")
//                    tmpFile.outputStream().use {
//                        picture.compress(Bitmap.CompressFormat.JPEG, 100, it)
//                    }
//                    handleImage(tmpFile.toUri())
//                }
//            }

    private val takePicture =
            registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
                if (success) handleImage(photoUri)
                else Toast.makeText(this, "Erreur ! 😢", Toast.LENGTH_LONG).show()
            }

    private val pickInGallery =
            registerForActivityResult(ActivityResultContracts.GetContent()) {
                if (it != null) {
                    handleImage(it)
                }
            }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        lifecycleScope.launch {
            val userInfo = Api.INSTANCE.userWebService.getInfo().body()!!
            profilePicture.load(userInfo.avatar) {
                transformations(CircleCropTransformation())
            }
        }

        uploadImageButton = findViewById(R.id.upload_image_button)
        uploadImageButton.setOnClickListener {
            pickInGallery.launch("image/*")
        }

        takePictureButton = findViewById(R.id.take_picture_button)
        takePictureButton.setOnClickListener {
            askCameraPermissionAndOpenCamera()
        }
    }

    private fun requestCameraPermission() =
        requestPermissionLauncher.launch(Manifest.permission.CAMERA)

    @RequiresApi(Build.VERSION_CODES.M)
    private fun askCameraPermissionAndOpenCamera() {
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                -> openCamera()
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                -> showExplanationDialog()
            else -> requestCameraPermission()
        }
    }

    private fun showExplanationDialog() {
        AlertDialog.Builder(this).apply {
            setMessage("On a besoin de la caméra sivouplé ! 🥺")
            setPositiveButton("Bon, ok") { _, _ ->
                requestCameraPermission()
            }
            setCancelable(true)
            show()
        }
    }

    private fun openCamera() = takePicture.launch(photoUri)

    private fun convert(uri: Uri) =
        MultipartBody.Part.createFormData(
            name = "avatar",
            filename = "temp.jpeg",
            body = contentResolver.openInputStream(uri)!!.readBytes().toRequestBody()
        )

    private fun handleImage(uri : Uri) {
        lifecycleScope.launch {
            urlPicture = Api.INSTANCE.userWebService.updateAvatar(convert(uri)).body()!!.avatar
            profilePicture.load(urlPicture) {
                transformations(CircleCropTransformation())
            }
        }
    }
}