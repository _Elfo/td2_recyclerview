package com.example.td2_recyclerview.AuthenticationFragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings.Global.putString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.example.td2_recyclerview.Login.Constants
import com.example.td2_recyclerview.Login.LoginForm
import com.example.td2_recyclerview.MainActivity
import com.example.td2_recyclerview.R
import com.example.td2_recyclerview.network.Api
import kotlinx.coroutines.launch

class LoginFragment: Fragment() {

    private lateinit var emailText: EditText
    private lateinit var passwordText: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        emailText = view.findViewById(R.id.email_editText)
        passwordText = view.findViewById(R.id.password_editText)

        val loginButton = view.findViewById<Button>(R.id.log_in_button)
        loginButton.setOnClickListener {
            if (emailText.text.toString().isNotBlank() && passwordText.text.toString().isNotBlank()) {
                val loginForm = LoginForm(emailText.text.toString(), passwordText.text.toString())
                lifecycleScope.launch {
                    val loginResponse = Api.INSTANCE.userWebService.login(loginForm)
                    if (loginResponse.isSuccessful) {
                        PreferenceManager.getDefaultSharedPreferences(context).edit {
                            putString(Constants.SHARED_PREF_TOKEN_KEY, loginResponse.body()!!.token)
                        }
                        startActivity(Intent(activity, MainActivity::class.java))
                    } else {
                        Toast.makeText(context, "Erreur ! Champ incorrect", Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                Toast.makeText(context, "Veuillez remplir tous les champs !", Toast.LENGTH_LONG).show()
            }
        }
    }
}