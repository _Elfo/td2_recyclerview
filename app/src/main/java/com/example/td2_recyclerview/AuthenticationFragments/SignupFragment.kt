package com.example.td2_recyclerview.AuthenticationFragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.example.td2_recyclerview.Login.Constants
import com.example.td2_recyclerview.MainActivity
import com.example.td2_recyclerview.R
import com.example.td2_recyclerview.Signup.SignUpForm
import com.example.td2_recyclerview.network.Api
import kotlinx.coroutines.launch

class SignupFragment: Fragment() {

    private lateinit var firstName: EditText
    private lateinit var lastName: EditText
    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var passwordConfirm: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_signup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        firstName = view.findViewById(R.id.firstName_editText)
        lastName = view.findViewById(R.id.lastName_editText)
        email = view.findViewById(R.id.email_editText)
        password = view.findViewById(R.id.password_editText)
        passwordConfirm = view.findViewById(R.id.confirmPassword_editText)

        val signupButton = view.findViewById<Button>(R.id.sign_up_button)
        signupButton.setOnClickListener {
            if (
                        firstName.text.toString().isNotBlank() &&
                        lastName.text.toString().isNotBlank() &&
                        email.text.toString().isNotBlank() &&
                        password.text.toString().isNotBlank() &&
                        passwordConfirm.text.toString().isNotBlank()
                    ) {
                if (password.text.toString() == passwordConfirm.text.toString()) {
                    val signupForm = SignUpForm(firstName.text.toString(), lastName.text.toString(), email.text.toString(), password.text.toString(), passwordConfirm.text.toString())
                    lifecycleScope.launch {
                        val signupResponse = Api.INSTANCE.userWebService.signup(signupForm)
                        if (signupResponse.isSuccessful) {
                            PreferenceManager.getDefaultSharedPreferences(context).edit {
                                putString(Constants.SHARED_PREF_TOKEN_KEY, signupResponse.body()!!.token)
                            }
                            startActivity(Intent(activity, MainActivity::class.java))
                        } else {
                            Toast.makeText(context, "Erreur ! Champ incorrect", Toast.LENGTH_LONG).show()
                        }
                    }
                } else {
                    Toast.makeText(context, "Veuillez choisir un mot de passe unique.", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(context, "Veuillez remplir tous les champs !", Toast.LENGTH_LONG).show()
            }
        }
    }
}