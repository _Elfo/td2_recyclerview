package com.example.td2_recyclerview.AuthenticationFragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.example.td2_recyclerview.MainActivity
import com.example.td2_recyclerview.R
import com.example.td2_recyclerview.network.Api
import com.example.td2_recyclerview.userinfo.UserInfoActivity

class AuthenticationFragment: Fragment() {
    private val taskIntent by lazy {
        Intent(activity, MainActivity::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_authentication, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (Api.getToken()!!.isNotBlank()) {
            startActivity(taskIntent)
        }

        val loginButton = view.findViewById<Button>(R.id.log_in_button)
        loginButton.setOnClickListener {
            findNavController().navigate(R.id.action_authenticationFragment_to_loginFragment)
        }

        val signupButton = view.findViewById<Button>(R.id.sign_up_button)
        signupButton.setOnClickListener {
            findNavController().navigate(R.id.action_authenticationFragment_to_signupFragment)
        }
    }
}