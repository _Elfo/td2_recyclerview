package com.example.td2_recyclerview.AuthenticationFragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.td2_recyclerview.R

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
    }
}