package com.example.td2_recyclerview.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserInfo(
    @SerialName("email")
    val email: String,
    @SerialName("firstname")
    val firstName: String,
    @SerialName("lastname")
    val lastName: String,
    @SerialName("avatar")
    val avatar: String = "https://goo.gl/gEgYUd"
) {
    companion object {
        const val USERINFO_REQUEST_CODE = 333
        const val PROFILE_PICTURE_KEY = "profile_picture_key"
    }
}