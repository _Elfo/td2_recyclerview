package com.example.td2_recyclerview.network

import android.content.Context
import android.provider.SyncStateContract
import androidx.preference.PreferenceManager
import com.example.td2_recyclerview.Login.Constants
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

class Api(private val context: Context) {

    companion object {
        private const val BASE_URL = "https://android-tasks-api.herokuapp.com/api/"
        public fun getToken() =
                PreferenceManager.getDefaultSharedPreferences(INSTANCE.context).getString(Constants.SHARED_PREF_TOKEN_KEY, "")
        lateinit var INSTANCE: Api
    }
//    private const val TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyODYsImV4cCI6MTYzODg4NTEyOX0.222wSDoxnbeBy7hnDgICaIkPhB249bn2OgIRRMFI1mo"

    val userWebService: UserWebService by lazy {
        retrofit.create(UserWebService::class.java)
    }

    val tasksWebService: TaskWebService by lazy {
        retrofit.create(TaskWebService::class.java)
    }

    // on construit une instance de parseur de JSON:
    private val jsonSerializer = Json {
        ignoreUnknownKeys = true
        coerceInputValues = true
    }

    // instance de convertisseur qui parse le JSON renvoyé par le serveur:
    private val converterFactory =
        jsonSerializer.asConverterFactory("application/json".toMediaType())

    // client HTTP
    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                // intercepteur qui ajoute le `header` d'authentification avec votre token:
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer ${getToken()}")
                    .build()
                chain.proceed(newRequest)
            }
            .build()
    }

    // permettra d'implémenter les services que nous allons créer:
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(converterFactory)
        .build()
}