package com.example.td2_recyclerview.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.td2_recyclerview.tasklist.Task

class TasksRepository {

    private val tasksWebService = Api.INSTANCE.tasksWebService

    suspend fun loadTasks(): List<Task>? {
        val response = tasksWebService.getTasks()
        return if (response.isSuccessful) response.body() else null
    }

    suspend fun removeTask(task: Task) : String? {
        val id = tasksWebService.deleteTask(task.id)
        return if (id.isSuccessful) id.body() else null
    }

    suspend fun createTask(task: Task) : Task? {
        val newTask = tasksWebService.createTask(task)
        return if (newTask.isSuccessful) newTask.body() else null
    }

    suspend fun updateTask(task: Task) : Task? {
        val editedTask = tasksWebService.updateTask(task)
        return if (editedTask.isSuccessful) editedTask.body() else null
    }

}