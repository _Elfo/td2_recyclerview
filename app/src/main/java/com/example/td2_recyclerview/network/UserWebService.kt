package com.example.td2_recyclerview.network

import com.example.td2_recyclerview.Login.LoginForm
import com.example.td2_recyclerview.Login.LoginResponse
import com.example.td2_recyclerview.Signup.SignUpForm
import com.example.td2_recyclerview.Signup.SignupResponse
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface UserWebService {
    @GET("users/info")
    suspend fun getInfo(): Response<UserInfo>

    @Multipart
    @PATCH("users/update_avatar")
    suspend fun updateAvatar(@Part avatar: MultipartBody.Part): Response<UserInfo>

    @POST("users/login")
    suspend fun login(@Body user: LoginForm): Response<LoginResponse>

    @POST("users/sign_up")
    suspend fun signup(@Body user: SignUpForm): Response<SignupResponse>
}