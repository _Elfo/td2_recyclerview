package com.example.td2_recyclerview.task

import android.os.Bundle
import android.widget.EditText
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.example.td2_recyclerview.R
import com.example.td2_recyclerview.tasklist.Task
import java.util.*

class TaskActivity : AppCompatActivity() {

    private val title by lazy {
        findViewById<EditText>(R.id.titleTask)
    }

    private val description by lazy {
        findViewById<EditText>(R.id.descriptionTask)
    }

    private val task by lazy {
        intent.getSerializableExtra(Task.TASK_KEY) as? Task ?:
        Task(id = UUID.randomUUID().toString(), title = "", description = "")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        title.setText(task.title)
        description.setText(task.description)

        val validateButton = findViewById<FloatingActionButton>(R.id.validateNewTask)
        validateButton.setOnClickListener {
            task.title = title.text.toString()
            task.description = description.text.toString()

            intent.putExtra(Task.TASK_KEY, task)
            setResult(Task.ADD_TASK_REQUEST_CODE, intent)
            finish()
        }
    }
}