package com.example.td2_recyclerview.Signup

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SignUpForm(
        @SerialName("firstname")
        val firstNam: String,
        @SerialName("lastname")
        val lastName: String,
        @SerialName("email")
        val email: String,
        @SerialName("password")
        val password: String,
        @SerialName("password_confirmation")
        val passwordConfirm: String
)