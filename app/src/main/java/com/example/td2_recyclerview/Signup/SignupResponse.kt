package com.example.td2_recyclerview.Signup

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SignupResponse(
        @SerialName("token")
        val token: String
)