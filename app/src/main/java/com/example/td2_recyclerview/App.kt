package com.example.td2_recyclerview

import android.app.Application
import com.example.td2_recyclerview.network.Api

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Api.INSTANCE = Api(this)
    }
}